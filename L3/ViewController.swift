//
//  ViewController.swift
//  L3
//
//  Created by Vadim on 10/9/18.
//  Copyright © 2018 Vadim. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        //вывести найбольшее из двух чисел
        largerNumber (a: 111, b: 1111)
        
        //вывести квдрат и куб числа
        numberInSquareAndCube (squareNumber: 2, cubeNumber: 2)
        
        //вывести от заданного и в обратном порядке до 0
        getNumbersUpAndDown(10)
        
        //найти общее количество делителей числа
        findDominators(50)
        
        //проверить совершенное число
        perfectNumber(29)
        
        //сколько денег на счету
        moneyOnBank(6)
        
    }
    
    //сколько денег на счету
    func moneyOnBank(_ procent: Double) {
        
        print("Block#2 Task # 1 \n")
        
        let initialPrice = 24 * Double(procent / 100) + 24
        var finalPrice = initialPrice
        
        for _ in 1826..<2018 {
            finalPrice = finalPrice + finalPrice * (Double(procent) / 100)
        }
        print(finalPrice)
        print("------------------------")
    }
    
    
    
    
    //проверить совершенное число
    func perfectNumber(_ count: UInt) {
        print("Block#1 Task # 5 \n")
        var summ = 0
        for i in 1..<count {
            let g = count % (i)
            if g == 0 {
                summ = summ + Int((i))
            }

        }
        if summ == count {
            print(count, "- Это совершенное число")
        }
        else {
            print(count, "- Это НЕсовершенное число")
        }
        print("------------------------")
    }
    
    //найти общее количество делителей числа
    func findDominators (_ count: UInt) {
        print("Block#1 Task # 4 \n")
        for i in 1..<count {
            let g = count % (i)
            if g == 0 {
                print(i)
            }
        }
        print("------------------------")
    }
    
    //вывести от заданного и в обратном порядке до 0
    func getNumbersUpAndDown (_ count: Int) {
        print("Block#1 Task # 3 \n")
        for i in 0..<count {
            print(i, terminator : " ");  print (abs(i - count + 1))
        }
        print("------------------------")
    }

    //вывести квадрат и куб числа
    func numberInSquareAndCube (squareNumber: UInt, cubeNumber: UInt) {
        print("Block#1 Task # 2 \n")
        print(squareNumber, "в квадрате = \(squareNumber * squareNumber);",  cubeNumber, "в кубе = \(cubeNumber * cubeNumber * cubeNumber)")
        print("------------------------")
    }
    
    //вывести найбольшее из двух чисел
    func largerNumber (a: UInt, b: UInt) {
        print("Block#1 Task # 1 \n")
        if a > b {
            print("Наибольшее число -", a)
        }
        else {
            print("Наибольшее число -", b)
        }
        print("------------------------")
    }

        
}

